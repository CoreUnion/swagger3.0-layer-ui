# Swagger3.0-layer-UI

#### 介绍
Swagger3.0 UI自动生成接口文档，不需要频繁更新接口文档，保证接口文档与代码的一致，

#### 软件架构
###  **来了就 Star下吧，方便下次查找
** 

#### 安装教程
1.拷贝WWWROOT文件到根目录既可，可以自定义导行路径
2.  由于代码已做了封装，使用好像不是太难，这里只做简单说明或参考示例源码配置。导航菜单及属性说明来源每个源码注释生成的XML文件
![输入图片说明](demo1.png)
![输入图片说明](demo2.png)
![输入图片说明](demo3.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

首先感谢开源的小伙伴liuhuan版本，在他的基础上重写支持swagger3.0协议规范。借用他的UI

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
