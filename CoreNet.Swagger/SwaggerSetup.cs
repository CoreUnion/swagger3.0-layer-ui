﻿using CoreNet.Swagger.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreNet.Swagger
{
    public static class SwaggerSetup
    {
        public static void AddSwaggerService(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            var apiName = "校池系统接口";

            services.AddSwaggerGen((s) =>
            {
                //遍历出全部的版本，做文档信息展示
                typeof(CustomApiVersion.ApiVersions).GetEnumNames().ToList().ForEach(version =>
                {
                    s.SwaggerDoc(version, new OpenApiInfo
                    {
                        Version = version,
                        Title = $"{apiName} 接口文档",
                        Description = $"{apiName} HTTP API " + version,
                        Contact = new OpenApiContact { Name = apiName, Email = "hzyibai88@163.com", Url = new Uri("https://bpool.cn") },
                        
                    });
                    s.OrderActionsBy(o => o.RelativePath);
                });
                
                //s.EnableAnnotations();
                try
                {
                    //生成API XML文档，为接口添加中文注释
                    var basePath = AppContext.BaseDirectory;
                    var xmlPath = Path.Combine(basePath, "DemoSwaggerUI.xml");
                    //var modelPath = Path.Combine(basePath, "CoreNet.Model.xml");
                    s.IncludeXmlComments(xmlPath);
                    //s.IncludeXmlComments(modelPath);
                }
                catch (Exception ex)
                {
                    //NLogUtil.WriteFileLog(NLog.LogLevel.Error, LogType.Swagger, "Swagger", "Swagger生成失败，Doc.xml丢失，请检查并拷贝。", ex);
                }

                s.DocumentFilter<AnnotationsDocumentFilter>();
                // 开启加权小锁
                s.OperationFilter<AddResponseHeadersFilter>();
                s.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();

                // 在header中添加token，传递到后台
                s.OperationFilter<SecurityRequirementsOperationFilter>();

                // 必须是 oauth2
                s.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "JWT授权(数据将在请求头中进行传输) 直接在下框中输入Bearer {token}（注意两者之间是一个空格）\"",
                    Name = "Authorization",//jwt默认的参数名称
                    In = ParameterLocation.Header,//jwt默认存放Authorization信息的位置(请求头中)
                    Type = SecuritySchemeType.ApiKey
                });

            });
        }
    }
}
