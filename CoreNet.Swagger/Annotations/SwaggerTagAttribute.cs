﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreNet.Swagger.Annotations
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class SwaggerTagAttribute : Attribute
    {
        /// <summary>
        /// Api分组标签
        /// </summary>
        /// <param name="name">分组名称</param>
        /// <param name="description">分组说明</param>
        /// <param name="docurl">外部文档</param>
        public SwaggerTagAttribute(string name,string description = null)
        {
            Description = description;
            Name = name;
        }
        /// <summary>
        /// 分组标签
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 分组标签说明
        /// </summary>
        public string Description { get; }
        /// <summary>
        /// 外部文档对象
        /// </summary>
        public string ExternalDocs { get;}
    }
}
