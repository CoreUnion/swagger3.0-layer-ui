﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSwaggerUI
{
    public class BaseModel
    {
        /// <summary>
        /// 表的主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 有效标识
        /// </summary>
        public int F_EnabledMark { get; set; }
        /// <summary>
        /// 子对
        /// </summary>
        public ModelInfo info { get; set; }
    }
    /// <summary>
    /// 子对
    /// </summary>
    public class ModelInfo
    {
        /// <summary>
        /// 删除标识
        /// </summary>
        public int F_DeleteMark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int F_Sort { get; set; }
    }
    /// <summary>
    /// 返回值类型
    /// </summary>
    public class AjaxResult
    {
        /// <summary>
        /// 消息类型
        /// </summary>
        public ResultType code { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string info { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public object data { get; set; }

        public AjaxResult()
        {

        }
        public AjaxResult(ResultType rt, string msg)
        {
            this.code = rt;
            this.info = msg;
            this.data = new object();
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="t">正常，成功，警告，错误</param>
        /// <param name="msg">内容</param>
        public AjaxResult(ResultType rt, string msg, object dt)
        {
            this.code = rt;
            this.info = msg;
            this.data = dt;
        }
    }

    /// <summary>
    /// 消息类型
    /// </summary>
    public enum ResultType
    {
        /// <summary>
        /// 成功显示
        /// </summary>
        success = 200,
        /// <summary>
        /// 重复数据
        /// </summary>
        repeat = 201,

        /// <summary>
        /// 超量
        /// </summary>
        excess = 202,
        /// <summary>
        /// 文档
        /// </summary>
        docauth = 203,
        /// <summary>
        /// 失败消息
        /// </summary>
        fail = 400,
        /// <summary>
        /// 异常提示
        /// </summary>
        exception = 500,
        /// <summary>
        /// 登陆失效
        /// </summary>
        nologin = 401,
    }
    /// <summary>
    /// API接口对象
    /// </summary>
    public class ApiModel
    {
        /// <summary>
        /// 基本属性
        /// </summary>
        public List<BaseModel> BaseInfo { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string F_Remarks { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        public string F_CreateName { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime F_CreateTime { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        public string F_UpdateName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime F_UpdateTime { get; set; }
    }
}
