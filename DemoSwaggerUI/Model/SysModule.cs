﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoSwaggerUI
{
    /// <summary>
    /// 栏目表
    /// </summary>
    public partial class SysModule
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 父级主键
        /// </summary>
        public string F_ParentId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string F_EnCode { get; set; }
        /// <summary>
        /// 栏目名称
        /// </summary>
        public string F_FullName { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string F_Icon { get; set; }
        /// <summary>
        /// 导航地址
        /// </summary>
        public string F_UrlAddress { get; set; }
        /// <summary>
        /// 导航目标
        /// </summary
        public string F_Target { get; set; }
        /// <summary>
        /// 菜单选项
        /// </summary>
        public int F_IsMenu { get; set; }
        /// <summary>
        ///是否展开
        /// </summary>
        public int F_AllowExpand { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int F_Sort { get; set; }
        /// <summary>
        /// 是否删除,0否,1是
        /// </summary>
        public int F_DeleteMark { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        public string F_CreateName { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime F_CreateTime { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        public string F_UpdateName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime F_UpdateTime { get; set; }        
        
    }
}
