﻿using CoreNet.Swagger.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSwaggerUI.Controllers
{
    /// <summary>
    /// 测试
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [SwaggerTag("系统管理", "说明文档")]
    public class SystemController : ControllerBase
    {
        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="entity">你看看</param>
        /// <returns>我知道</returns>
        [HttpPost]
        [Authorize]
        //[SwaggerOperation("获取用户","用户说明",)]
        public SysRole GetTaxCode([FromBody] SysModule entity)
        {

            return null;
        }


        /// <summary>
        /// GetStrInfo
        /// </summary>
        /// <param name="info">参数1</param>
        /// <param name="value">参数2</param>
        /// <returns></returns>
        [HttpGet]
        public string GetRos(string info, string value = "wang")
        {
            return "demo";
        }
        /// <summary>
        /// 获取用户23
        /// </summary>
        /// <param name="entity">你看看4</param>
        /// <returns>我知道55</returns>
        [HttpPost]
        public List<SysRole> GetsRole(SysRole entity)
        {

            return null;
        }
        /// <summary>
        /// 嵌套实体
        /// </summary>
        /// <param name="role">角色</param>
        /// <param name="Id">对象ID</param>
        /// <returns></returns>
        [HttpGet]
        public ApiModel GetApi([FromBody] ApiModel role, int Id)
        {
            return new ApiModel();
        }


        /*
        测试parameters类型
        1.基本类型测试，int,string,datetime,默认值
        2.arr[int],arr[string]
        测试requestBody类型
        1.基本类型测试，int,string,datetime,默认值
        2.对象, arr[对象],嵌套对象 三种情况

         */
    }
}
