﻿using CoreNet.Swagger.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoSwaggerUI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [SwaggerTag("系统初始", "说明文档")]
    public class HomeController : ControllerBase
    {

        /// <summary>
        /// 无参请求
        /// </summary>
        /// <returns>返回结果Array</returns>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// 获取注释
        /// </summary>
        /// <param name="id">湖南ID</param>
        /// <returns>返回结果</returns>
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// PostFrom
        /// </summary>
        /// <param name="value">字符串</param>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }


        /// <summary>
        /// PutForm
        /// </summary>
        /// <param name="id">整型ID</param>
        /// <param name="value">字符串</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">ID</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
